// https://github.com/michael-ciniawsky/postcss-load-config

module.exports = {
  "plugins": {
    "precss": {},
    "postcss-import": {},
    "postcss-url": {},
    "postcss-cssnext": {
      browsers: ['last 2 versions', '> 5%'],
    }
  },

  map: true

}
