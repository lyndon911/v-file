# v-file

> 使用vue.js 开发的在线文件管理器


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## 功能
* 文件列表  √
* 文件图标  -->80%
* 重命名    √
* 上传      -->60%
* 路径      90%
* 路径访问  √
* 文件夹移动(单)  √
* 文件夹移动(多)  0%
* 文件移动(单)    √
* 文件移动(多)0%
* 文件夹复制 0%
* 文件复制 0%
* 切换显示样式 100%
* 右键菜单  80%
* 排序   0%
* 删除   90%  需要添加提示
* 文件下载  100%
* 新建文件夹 90%
