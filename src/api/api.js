import axios from 'axios'

// axios.interceptors.request.use(function (config) {
//   // 处理请求之前的配置
//   if (config.method === 'post') {
//     config.data = JSON.stringify(config.data)
//   } else if (config.method === 'get') {
//     // 给GET 请求后追加时间戳， 解决IE ajax 请求缓存问题
//     let symbol = config.url.indexOf('?') > 0 ? '&' : '?'
//     config.url += symbol + Date.now()
//   }
//   return config
// }, function (error) {
//   // 请求失败的处理
//   return Promise.reject(error)
// })

const base = '/api';
export const FileList = path => {
  return axios.get(`${base}/listFile`, {
    params: {'path': path}
  }).then(res => res.data)
};

export const FileUpload = (file, path, progress) => {
  let formData = new FormData();
  formData.append('file', file);
  formData.append('path', path);
  return axios.post(`${base}/uploadFile`, formData, {
    headers: {
      'content-type': 'multipart/form-data;boundary=uploadFile'
    },
    'onUploadProgress': progress
  }).then(res => res.data)
};


export const FileReName = (obj) => {
  return axios.post(`${base}/rename`, {
    oldPath: obj.oldPath,
    newPath: obj.newPath
  }).then(res => res.data)
};

export const FileMove = (oldPath, newPath) => {
  return axios.post(`${base}/rename`, {
    oldPath: oldPath,
    newPath: newPath
  }).then(res => res.data)
};

export const CreateFolder = (path, dirName) => {
  return axios.post(`${base}/mkdir`, {
    path: path,
    dirName: dirName
  }).then(res => res.data);
};

export const FileRemove = (path) => {
  return axios.post(`${base}/remove`, {
    path: path
  }).then(res => res.data);
};
